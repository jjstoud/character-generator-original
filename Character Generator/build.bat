@echo off
cls

set EXE_NAME=interpolate
del %EXE_NAME%.exe
del %EXE_NAME%.obj
del %EXE_NAME%.lst
del %EXE_NAME%.ilk
del %EXE_NAME%.pdb

set DRIVE_LETTER=%1:
set PATH=%DRIVE_LETTER%\Assembly\masm;c:\Windows;c:\Windows\system32
set INCLUDE=%DRIVE_LETTER%\Assembly
set LIB_DIRS=%DRIVE_LETTER%\Assembly

ml -c -coff -Zi interpolate_driver.asm
ml -c -coff -Zi interpolate.asm
ml -c -coff -Zi compute_bs.asm

link /libpath:%LIB_DIRS% interpolate_driver.obj interpolate.obj interpolate_sort.obj compute_bs.obj ftoaproc.obj atofproc.obj compare_floats.obj io.obj kernel32.lib /debug /out:interpolate.exe /subsystem:console /entry:start
%EXE_NAME% < points.txt