﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Character_Generator
{
    public partial class GUI : Form
    {
        MaleCharacters charMale;
        FemaleCharacters charFemale;
        SaveFileDialog saveFileName;

        public GUI()
        {
            InitializeComponent();
            saveFileName = new SaveFileDialog();
            saveFileName.Filter = "Text Files | *.txt";
            saveFileName.DefaultExt = "txt";
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            if (comboBoxGender.SelectedItem.ToString() == "Male")
            {
                if (comboBoxClass.SelectedItem.ToString() == "Warrior")
                {
                    charMale = new MaleCharacters("Warrior");
                }

                else if (comboBoxClass.SelectedItem.ToString() == "Mage")
                {
                    charMale = new MaleCharacters("Mage");
                }

                else if (comboBoxClass.SelectedItem.ToString() == "Rogue")
                {
                    charMale = new MaleCharacters("Rogue");
                }

                else if (comboBoxClass.SelectedItem.ToString() == "Cleric")
                {
                    charMale = new MaleCharacters("Cleric");
                }

                charMale.generateStats();
                appendMale(charMale);
            }

            else if (comboBoxGender.SelectedItem.ToString() == "Female")
            {
                if (comboBoxClass.SelectedItem.ToString() == "Warrior")
                {
                    charFemale = new FemaleCharacters("Warrior");
                }

                else if (comboBoxClass.SelectedItem.ToString() == "Mage")
                {
                    charFemale = new FemaleCharacters("Mage");
                }

                else if (comboBoxClass.SelectedItem.ToString() == "Rogue")
                {
                    charFemale = new FemaleCharacters("Rogue");
                }

                else if (comboBoxClass.SelectedItem.ToString() == "Cleric")
                {
                    charFemale = new FemaleCharacters("Cleric");
                }

                charFemale.generateStats();
                appendFemale(charFemale);
            }

            buttonSave.Enabled = true;
        }

        private void appendMale(MaleCharacters charMale)
        {
            textBoxName.Text = charMale.getName();
            labelHealth.Text = "Health: " + charMale.getHealth();
            labelStaMan.Text = "Mana/Stamina: " + charMale.getStaMan();
            labelStrength.Text = "Strength: " + charMale.getStrength();
            labelConstitution.Text = "Constitution: " + charMale.getConstitution();
            labelDexterity.Text = "Dexterity: " + charMale.getDexterity();
            labelCharisma.Text = "Charisma: " + charMale.getCharisma();
            labelIntelligence.Text = "Intelligence: " + charMale.getIntelligence();
            labelWisdom.Text = "Wisdom: " + charMale.getWisdom();
            labelResistance.Text = "Resistance: " + charMale.getResistance();
            labelLuck.Text = "Luck: " + charMale.getLuck();
        }

        private void appendFemale(FemaleCharacters charFemale)
        {
            textBoxName.Text = charFemale.getName();
            labelHealth.Text = "Health: " + charFemale.getHealth();
            labelStaMan.Text = "Mana/Stamina: " + charFemale.getStaMan();
            labelStrength.Text = "Strength: " + charFemale.getStrength();
            labelConstitution.Text = "Constitution: " + charFemale.getConstitution();
            labelDexterity.Text = "Dexterity: " + charFemale.getDexterity();
            labelCharisma.Text = "Charisma: " + charFemale.getCharisma();
            labelIntelligence.Text = "Intelligence: " + charFemale.getIntelligence();
            labelWisdom.Text = "Wisdom: " + charFemale.getWisdom();
            labelResistance.Text = "Resistance: " + charFemale.getResistance();
            labelLuck.Text = "Luck: " + charFemale.getLuck();
        }
        
        private void comboBoxGender_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkParameters())
            {
                buttonGenerate.Enabled = true;
            }

            else
            {
                buttonGenerate.Enabled = false;
            }
        }

        private void comboBoxClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkParameters())
            {
                buttonGenerate.Enabled = true;
            }

            else
            {
                buttonGenerate.Enabled = false;
            }
        }

        private bool checkParameters()
        {
            if (comboBoxGender.SelectedItem != null && comboBoxClass.SelectedItem != null)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
           try
           {
               if (comboBoxGender.SelectedItem.ToString() == "Male")
               {
                   saveFileName.FileName = charMale.getName() + ".txt";
               }

               else if (comboBoxGender.SelectedItem.ToString() == "Female")
               {
                   saveFileName.FileName = charFemale.getName() + ".txt";
               }

               DialogResult result = saveFileName.ShowDialog();

               if (result == DialogResult.Cancel)
               {
                   return;
               }

               System.IO.Stream fileStream = saveFileName.OpenFile();
               System.IO.StreamWriter sw = new System.IO.StreamWriter(fileStream);
               
               if (comboBoxGender.SelectedItem.ToString() == "Male")
               {
                   sw.WriteLine("Name: " + charMale.getName());
                   sw.WriteLine("Gender: Male");
                   sw.WriteLine("Class: " + charMale.getJob());
                   sw.WriteLine("Health: " + charMale.getHealth());
                   sw.WriteLine("Mana/Stamina: " + charMale.getStaMan());
                   sw.WriteLine("Strength: " + charMale.getStrength());
                   sw.WriteLine("Constitution: " + charMale.getConstitution());
                   sw.WriteLine("Dexterity: " + charMale.getDexterity());
                   sw.WriteLine("Charisma: " + charMale.getCharisma());
                   sw.WriteLine("Intelligence: " + charMale.getIntelligence());
                   sw.WriteLine("Wisdom: " + charMale.getWisdom());
                   sw.WriteLine("Resistance: " + charMale.getResistance());
                   sw.Write("Luck: " + charMale.getLuck());
               }

               else if (comboBoxGender.SelectedItem.ToString() == "Female")
               {
                   sw.WriteLine("Name: " + charFemale.getName());
                   sw.WriteLine("Gender: Female");
                   sw.WriteLine("Class: " + charFemale.getJob());
                   sw.WriteLine("Health: " + charFemale.getHealth());
                   sw.WriteLine("Mana/Stamina: " + charFemale.getStaMan());
                   sw.WriteLine("Strength: " + charFemale.getStrength());
                   sw.WriteLine("Constitution: " + charFemale.getConstitution());
                   sw.WriteLine("Dexterity: " + charFemale.getDexterity());
                   sw.WriteLine("Charisma: " + charFemale.getCharisma());
                   sw.WriteLine("Intelligence: " + charFemale.getIntelligence());
                   sw.WriteLine("Wisdom: " + charFemale.getWisdom());
                   sw.WriteLine("Resistance: " + charFemale.getResistance());
                   sw.Write("Luck: " + charFemale.getLuck());
               }

               sw.Flush();
               sw.Close();
           }

           catch (InvalidOperationException ex)
           {
               DialogResult dialog = MessageBox.Show("Error saving file!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
           }
               
        }
    }
}
