﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Character_Generator
{
    class MaleCharacters
    {
        String charJob;
        Warrior warrior;
        Mage mage;
        Rogue rogue;
        Cleric cleric;

        public MaleCharacters(String job)
        {
            charJob = job;
            warrior = new Warrior("Male");
            mage = new Mage("Male");
            rogue = new Rogue("Male");
            cleric = new Cleric("Male");
        }

        public void generateStats()
        {
            if (charJob == "Warrior")
            {
                warrior.setStats();
            }

            else if (charJob == "Mage")
            {
                mage.setStats();
            }

            else if (charJob == "Rogue")
            {
                rogue.setStats();
            }

            else
            {
                cleric.setStats();
            }
        }

        public int getHealth()
        {
            if (charJob == "Warrior")
            {
                return warrior.getHealth();
            }

            else if (charJob == "Mage")
            {
                return mage.getHealth();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getHealth();
            }

            else
            {
                return cleric.getHealth();
            }
        }

        public int getStaMan()
        {
            if (charJob == "Warrior")
            {
                return warrior.getStaMan();
            }

            else if (charJob == "Mage")
            {
                return mage.getStaMan();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getStaMan();
            }

            else
            {
                return cleric.getStaMan();
            }
        }

        public int getStrength()
        {
            if (charJob == "Warrior")
            {
                return warrior.getStrength();
            }

            else if (charJob == "Mage")
            {
                return mage.getStrength();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getStrength();
            }

            else
            {
                return cleric.getStrength();
            }
        }

        public int getConstitution()
        {
            if (charJob == "Warrior")
            {
                return warrior.getConstitution();
            }

            else if (charJob == "Mage")
            {
                return mage.getConstitution();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getConstitution();
            }

            else
            {
                return cleric.getConstitution();
            }
        }

        public int getDexterity()
        {
            if (charJob == "Warrior")
            {
                return warrior.getDexterity();
            }

            else if (charJob == "Mage")
            {
                return mage.getDexterity();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getDexterity();
            }

            else
            {
                return cleric.getDexterity();
            }
        }

        public int getCharisma()
        {
            if (charJob == "Warrior")
            {
                return warrior.getCharisma();
            }

            else if (charJob == "Mage")
            {
                return mage.getCharisma();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getCharisma();
            }

            else
            {
                return cleric.getCharisma();
            }
        }

        public int getIntelligence()
        {
            if (charJob == "Warrior")
            {
                return warrior.getIntelligence();
            }

            else if (charJob == "Mage")
            {
                return mage.getIntelligence();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getIntelligence();
            }

            else
            {
                return cleric.getIntelligence();
            }
        }

        public int getWisdom()
        {
            if (charJob == "Warrior")
            {
                return warrior.getWisdom();
            }

            else if (charJob == "Mage")
            {
                return mage.getWisdom();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getWisdom();
            }

            else
            {
                return cleric.getWisdom();
            }
        }

        public int getResistance()
        {
            if (charJob == "Warrior")
            {
                return warrior.getResistance();
            }

            else if (charJob == "Mage")
            {
                return mage.getResistance();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getResistance();
            }

            else
            {
                return cleric.getResistance();
            }
        }

        public int getLuck()
        {
            if (charJob == "Warrior")
            {
                return warrior.getLuck();
            }

            else if (charJob == "Mage")
            {
                return mage.getLuck();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getLuck();
            }

            else
            {
                return cleric.getLuck();
            }
        }

        public string getName()
        {
            if (charJob == "Warrior")
            {
                return warrior.getName();
            }

            else if (charJob == "Mage")
            {
                return mage.getName();
            }

            else if (charJob == "Rogue")
            {
                return rogue.getName();
            }

            else
            {
                return cleric.getName();
            }
        }

        public string getJob()
        {
            return charJob;
        }
    }
}
