﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Character_Generator
{
    interface IClass
    {
        void setStats();
        void setHealth();
        void setStaMan();
        void setStrength();
        void setConstitution();
        void setDexterity();
        void setCharisma();
        void setIntelligence();
        void setWisdom();
        void setResistance();
        void setLuck();
        void setMaleName();
        void setFemaleName();
        int getHealth();
        int getStaMan();
        int getStrength();
        int getConstitution();
        int getDexterity();
        int getCharisma();
        int getIntelligence();
        int getWisdom();
        int getResistance();
        int getLuck();
        string getName();
    }
}
