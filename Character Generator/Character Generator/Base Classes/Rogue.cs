﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Character_Generator
{
    class Rogue : IClass
    {
        String charGen;
        string name = "";
        string mName = "";
        string fName = "";
        int[] arrayStats = new int[10];
        ArrayList maleWarriorNames = new ArrayList();
        ArrayList femaleWarriorNames = new ArrayList();
        Random rand = new Random();

        public Rogue(String gender)
        {
            charGen = gender;
        }

        public void setStats()
        {
            setHealth();
            setStaMan();
            setStrength();
            setConstitution();
            setDexterity();
            setCharisma();
            setIntelligence();
            setWisdom();
            setResistance();
            setLuck();

            if (charGen == "Male")
            {
                setMaleName();
            }

            else if (charGen == "Female")
            {
                setFemaleName();
            }
        }
        public void setHealth()
        {
            arrayStats[0] = rand.Next(60, 81);
        }

        public void setStaMan()
        {
            arrayStats[1] = rand.Next(30, 41);
        }

        public void setStrength()
        {
            arrayStats[2] = rand.Next(7, 13);
        }

        public void setConstitution()
        {
            arrayStats[3] = rand.Next(5, 11);
        }

        public void setDexterity()
        {
            arrayStats[4] = rand.Next(10, 16);
        }

        public void setCharisma()
        {
            arrayStats[5] = rand.Next(0, 11);
        }

        public void setIntelligence()
        {
            arrayStats[6] = rand.Next(2, 8);
        }

        public void setWisdom()
        {
            arrayStats[7] = rand.Next(0, 11);
        }

        public void setResistance()
        {
            arrayStats[8] = rand.Next(2, 8);
        }

        public void setLuck()
        {
            arrayStats[9] = rand.Next(0, 11);
        }

        public void setMaleName()
        {
            int counter = 0;

            string path = Directory.GetCurrentDirectory();
            System.IO.StreamReader file = new System.IO.StreamReader(path + "\\Names\\MaleRogueNames.txt");

            while ((name = file.ReadLine()) != null)
            {
                maleWarriorNames.Add(name);
                counter++;
            }

            file.Close();

            String[] temp = (String[])maleWarriorNames.ToArray(typeof(string));
            int index = rand.Next(temp.Length);
            mName = temp[index];
        }

        public void setFemaleName()
        {
            int counter = 0;

            string path = Directory.GetCurrentDirectory();
            System.IO.StreamReader file = new System.IO.StreamReader(path + "\\Names\\FemaleRogueNames.txt");

            while ((name = file.ReadLine()) != null)
            {
                femaleWarriorNames.Add(name);
                counter++;
            }

            file.Close();

            String[] temp = (String[])femaleWarriorNames.ToArray(typeof(string));
            int index = rand.Next(temp.Length);
            fName = temp[index];
        }

        public int getHealth()
        {
            return arrayStats[0];
        }

        public int getStaMan()
        {
            return arrayStats[1];
        }

        public int getStrength()
        {
            return arrayStats[2];
        }

        public int getConstitution()
        {
            return arrayStats[3];
        }

        public int getDexterity()
        {
            return arrayStats[4];
        }

        public int getCharisma()
        {
            return arrayStats[5];
        }

        public int getIntelligence()
        {
            return arrayStats[6];
        }

        public int getWisdom()
        {
            return arrayStats[7];
        }

        public int getResistance()
        {
            return arrayStats[8];
        }

        public int getLuck()
        {
            return arrayStats[9];
        }

        public string getName()
        {
            if (charGen == "Male")
            {
                return mName;
            }

            else
            {
                return fName;
            }
        }
    }
}
